Рукомендации под *nix системы

Для простоты работы со стилями, используется сборщик пакетов [GULP](https://gulpjs.com/). 
Стили прописанны через SASS(SCSS).

### 1. Установка 
* `npm install -g gulp` 
* `composer install`

### 2. Запуск сборки 
* `gulp watch` - просмотр файлов и создание сборки при изменений файлов (сейчас пакет настроен на папку `www/asset/style` болле подробно можно ознакомится посмотрев файл `gulpfile.js`)
* `gulp build` - собирает пакет стилей

Готовая сборка будет собиратся в `www/build/style.css`.

### 3. Подключение
Стили к основному сайту были синхронизированы на 02.07

##### 3.1
В haed нужно будет разместить подключение стилией и заменить тег `viewport`
```php
<meta name="viewport" content="width=device-width">
<link href="/www/build/style.css" rel="stylesheet">
```
##### 3.2
На тег body нужно будет прописать доп.класс (`page-mobile`)на всех страницах. Класс `page-en-mobile` нужно прописать только на EN (новой версии сайта), при этом класс `page-mobile` так же должен присутствовать
* page-mobile - облочка для распознование стилией для мобильных устройств
* page-en-mobile - оболочка для EN версии сайта
```php
<body class="page-mobile page-en-mobile">
```
##### 3.2
В верстке могу встретися новые классы `js-*`.
Ознокомится с тем как они работают можно в `www/asset/script/mobile.js`
* js-mobile-content - манипулирование контентом при открытов меню 
* js-checkbox - управление чекбоксами
* js-modal-close - закрыть модальное окно (`$(this).attr('data-form')`)
* js-modal-open - открыть модальное окно (`$($(this).attr('href'));`)
* js-menu-nav - управление открытие\закритем мобильного меню
* js-no-banner - фиксирует высоту блоков если нету банера
* js-swap-grid - меняет местами вложены блоки
```php
<div class="b-common-wrap js-mobile-content">
```
##### 3.2
Перед закрытием body нужно будет подключить скрипты
```php
//
<script src="/www/asset/script/mobile.js"></script>
// Подключать на страницах с играми
<script src="/www/asset/script/mobile_game.js"></script>
// Подключение масски к input
<script src="/www/main/jquery.mask.min.js"></script>
<script>
    $('#card-number').mask('0000-0000-0000-0000-00');
</script>
```

### Список страниц и компонентов

```php
Доступ к страницам
login: krem
password: krem2000
```

* [Page: Home (NoAuth)](http://crea.shiroorg.ru/www/home.html)
* [Page: Home (Auth)](http://crea.shiroorg.ru/www/home_auth.html)
* [Блог: Текст](http://crea.shiroorg.ru/www/blog-text.html)
* [Payments: Iframe](http://crea.shiroorg.ru/www/iframe-payments.html)
* [Game: Iframe](http://crea.shiroorg.ru/www/iframe-game.html)
* [Modal EN: Confirm](http://crea.shiroorg.ru/www/modal-en-confirm.html)
* [Modal EN: Notice](http://crea.shiroorg.ru/www/modal-en-notice.html)
* [Modal EN: Payments](http://crea.shiroorg.ru/www/modal-en-payment.html)
* [Modal EN: Payments 2](http://crea.shiroorg.ru/www/modal-en-payment-2.html)
* [Modal EN: Payments 3](http://crea.shiroorg.ru/www/modal-en-payment-3.html)
* [Modal EN: Support](http://crea.shiroorg.ru/www/modal-en-support.html)
* [Modal: Login](http://crea.shiroorg.ru/www/modal-login.html)
* [Modal: Nickname](http://crea.shiroorg.ru/www/modal-nickname.html)
* [Modal: Recover](http://crea.shiroorg.ru/www/modal-recover.html)
* [Modal: SignUp](http://crea.shiroorg.ru/www/modal-signup.html)
* [Modal: SignUp Error](http://crea.shiroorg.ru/www/modal-signup.html)
* [Modal: Support](http://crea.shiroorg.ru/www/modal-support.html)
* [Modal: Support 2](http://crea.shiroorg.ru/www/modal-support-2.html)
* [Page: Questions](http://crea.shiroorg.ru/www/page-en-popular-questions.html)
* [Page EN: Support](http://crea.shiroorg.ru/www/page-en-support.html)
* [Page: Game](http://crea.shiroorg.ru/www/page-game.html)
* [Profile EN: Games](http://crea.shiroorg.ru/www/profile-en-games.html)
* [Profile EN: Setting](http://crea.shiroorg.ru/www/profile-en-setting.html)
* [Profile EN: Support](http://crea.shiroorg.ru/www/profile-en-support.html)
* [Profile: Games](http://crea.shiroorg.ru/www/profile-games.html)
* [Profile: Pay](http://crea.shiroorg.ru/www/profile-pay-amounts.html)
* [Profile: Setting](http://crea.shiroorg.ru/www/profile-setting.html)

### @ideaFix 
```php
//Todo: Вообще в "основном проекте" желательно выпилить подобное использование картинок, 
если начали использовать спрайты то лучше их объединить до конца
приходится переносит костыли - *::icon
```