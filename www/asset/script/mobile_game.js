/*
Так как браузеры блокируют подобные действия, мы стараемся их обойти.
 */
$(function () {
    if (!screenfull.enabled) {
        return false;
    }

    $('#load-game').click(function () {
        var target = $("#container");
        target.html($("#template-iframe").html());

        if (target.requestFullscreen) {
            target.requestFullscreen();
        } else if (target.msRequestFullscreen) {
            target.msRequestFullscreen();
        } else if (target.mozRequestFullScreen) {
            target.mozRequestFullScreen();
        } else if (target.webkitRequestFullscreen) {
            target.webkitRequestFullscreen();
        } else {
            screenfull.request(target[0]).then(function () {
                console.log('Browser entered fullscreen mode')
            })
        }
    });

    // Иницируем открытие, только для мобилок (version 1, с подгрузкой iframe)
    $('#load-game').click(function () {

    });

    // Иницируем открытие, только для мобилок (version 2, может быть заблокирован на строне браузера)
    /*$(document).ready(function () {
        if($( document ).width() <= 768) {
            setTimeout(function () {
                $('#request').trigger('click');
            }, 1000);
        }
    });*/
});