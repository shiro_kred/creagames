;(function (document, window, index){
    'use strict';
    var inputs = document.querySelectorAll('.inputfile');
    Array.prototype.forEach.call(inputs, function (input) {
        var label = input.nextElementSibling,
            labelVal = label.innerHTML;

        input.addEventListener('change', function (e) {
            var fileName = '';
            if (this.files && this.files.length > 1)
                fileName = ( this.getAttribute('data-multiple-caption') || '' ).replace('{count}', this.files.length);
            else
                fileName = e.target.value.split('\\').pop();

            if (fileName)
                label.querySelector('span').innerHTML = fileName;
            else
                label.innerHTML = labelVal;
        });

        // Firefox bug fix
        input.addEventListener('focus', function () {
            input.classList.add('has-focus');
        });
        input.addEventListener('blur', function () {
            input.classList.remove('has-focus');
        });
    });
}(document, window, 0));

/* Управление чекбоксами */
$(".js-checkbox").click(function (e) {
    var checkbox_target = $('#'+$(this).attr('data-form'));
    if($(this).hasClass('act') == false) {
        $(this).addClass('act');
        $('.js-checkbox').addClass('act');
        checkbox_target.addClass('checked');
        checkbox_target.attr('checked', 'true');
        checkbox_target.val(1);
    } else {
        $(this).removeClass('act');
        $('.js-checkbox').removeClass('act');
        checkbox_target.removeClass('checked');
        checkbox_target.attr('checked', false);
        checkbox_target.val(0);
    }
});

/* Управление закрытием модальных окон */
$(".js-modal-close").click(function (e) {
    var modal_target = $('#'+$(this).attr('data-form'));
    $(".b-page-content").removeClass('is-hidden');
    $(".js-footer").removeClass('is-hidden');
    modal_target.removeClass('_visible');
    modal_target.removeClass('visible');
});

/* Управление открытием модальных окон */
$(".js-modal-open").click(function (e) {
    e.preventDefault();
    var modal_target = $($(this).attr('href'));
    $(".im-popup").removeClass('_visible');
    $(".im-popup").removeClass('visible');
    modal_target.addClass('_visible');
    modal_target.addClass('visible');

    $(".has_submenu").removeClass('active');
    $('#'+$(".has_submenu").attr('data-form')).removeClass('active');
    $(".js-mobile-menu").removeClass('active');
    $(".js-menu-nav").removeClass('active');
    $(".menu__container").addClass('is-hidden');
    $(".b-page-content").addClass('is-hidden');
    $(".js-footer").addClass('is-hidden');
});



/* Управление открытие\закрытеие вложеных игр в мобильное меню */
$(".has_submenu").click(function (e) {
    var menu_target = $('#'+$(this).attr('data-form'));
    menu_target.toggleClass('active');
    $(this).toggleClass('active');
});

/* Управление открытие\закритем мобильного меню */
$(".js-menu-nav").click(function () {

    // Если меню закрываем и надо выполнить доп.действие
    if($(this).hasClass('active') == true) {
        $(".has_submenu").removeClass('active');
        $('#'+$(".has_submenu").attr('data-form')).removeClass('active');
    }

    $(".js-mobile-menu").toggleClass('active');
    $(".js-menu-nav").toggleClass('active');
    $(".menu__container").toggleClass('is-hidden');
});

$(".g-title-head").click(function () {
    var target = $('.g-questions-head[data-block="'+$(this).parent('.g-questions-head').attr('data-block')+'"]');

    if(target.hasClass('show') == false) {
        setTimeout(function () {
            target.addClass('show');
        }, 100);
    }
    $('.g-questions-head').removeClass('show');

});
$('.js-questions-show').click(function() {
    $(this).toggleClass('active').next()[$(this).next().is(':hidden') ? "slideDown" : "slideUp"](400);
});

/* Масска и переключение инпутов для карты*/
$('.b-card').mask('0000');
$('.b-card').keyup(function () {
    var count = $(this).val().length;
    if (count == 4) {
        var target = parseInt($(this).attr('data-target')) + 1;
        $('.b-card[data-target="' + target + '"]').focus();
    }
});