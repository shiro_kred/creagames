
var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('build', function () {
    return gulp.src('www/asset/style/**/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('www/build'))
});

gulp.task('watch', function(){
    gulp.watch('www/asset/style/**/*.scss', ['build']);
});
